# keitetsu’s German Localization

Provides german localization for various mods.

## Building this mod

```sh
bundle install
rake
```

## Translated Mods

* Angel's Mods:
    * [Angel's Bio Processing](https://mods.factorio.com/mod/angelsbioprocessing)
    * [Angel's Infinite Ores](https://mods.factorio.com/mod/angelsinfiniteores)
    * [Angel's Petro Chemical Processing](https://mods.factorio.com/mod/angelspetrochem)
    * [Angel's Refining](https://mods.factorio.com/mod/angelsrefining)
    * [Angel's Smelting](https://mods.factorio.com/mod/angelssmelting)
* Bob's Mods:
    * [Bob's Adjustable Inserters](https://mods.factorio.com/mod/bobinserters)
    * [Bob's Assembling machines](https://mods.factorio.com/mod/bobassembly)
    * [Bob's Electronics](https://mods.factorio.com/mod/bobelectronics)
    * [Bob's Enemies](https://mods.factorio.com/mod/bobenemies)
    * [Bob's Greenhouse mod](https://mods.factorio.com/mod/bobgreenhouse)
    * [Bob's Logistics mod](https://mods.factorio.com/mod/boblogistics)
    * [Bob's Metals, Chemicals and Intermediates](https://mods.factorio.com/mod/bobplates)
    * [Bob's Mining](https://mods.factorio.com/mod/bobmining)
    * [Bob's Modules](https://mods.factorio.com/mod/bobmodules)
    * [Bob's Ores](https://mods.factorio.com/mod/bobores)
    * [Bob's Power](https://mods.factorio.com/mod/bobpower)
    * [Bob's Revamp mod](https://mods.factorio.com/mod/bobrevamp)
    * [Bob's Tech](https://mods.factorio.com/mod/bobtech)
    * [Bob's Vehicle Equipment](https://mods.factorio.com/mod/bobvehicleequipment)
    * [Bob's Warfare](https://mods.factorio.com/mod/bobwarfare)

## License [![](http://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)][license]

Copyright © Oliver Stotzem

Licensed under the [MIT license][license].

[license]: LICENSE.md
