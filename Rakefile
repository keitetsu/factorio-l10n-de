# coding: utf-8
## Build script for keitetsu-l10n-de
#  Author:     Oliver Stotzem
#  Repository: https://github.com/keitetsu/factorio-l10n-de
#-------------------------------------------------------------------------------
#  Copyright © Oliver Stotzem
#
#  See the file LICENSE.md for copying permission.

info = {
  title:            'keitetsu‘s German Localization',
  name:             'keitetsu-l10n-de',
  description:      'Provides german localization for '\
                    'Angel‘s Mods & Bob‘s Mods',
  homepage:         'https://github.com/keitetsu/factorio-l10n-de',
  author:           'keitetsu',
  version:          '0.05.0',
  factorio_version: '0.16',
  dependencies:     []
}
 
dependencies = [
  [ "angelsbioprocessing", "0.5.4"  ],
  [ "angelsinfiniteores",  "0.7.3"  ],
  [ "angelspetrochem",     "0.7.6"  ],
  [ "angelsrefining",      "0.9.9"  ],
  [ "angelssmelting",      "0.4.3"  ],
  [ "bobassembly",         "0.16.1" ],
  [ "bobelectronics",      "0.16.0" ],
  [ "bobenemies",          "0.16.0" ],
  [ "bobgreenhouse",       "0.16.0" ],
  [ "bobinserters",        "0.16.5" ],
  [ "boblogistics",        "0.16.7" ],
  [ "bobmining",           "0.16.0" ],
  [ "bobmodules",          "0.16.0" ],
  [ "bobores",             "0.16.2" ],
  [ "bobplates",           "0.16.1" ],
  [ "bobpower",            "0.16.2" ],
  [ "bobrevamp",           "0.16.1" ],
  [ "bobtech",             "0.16.1" ],
  [ "bobvehicleequipment", "0.16.2" ],
  [ "bobwarfare",          "0.16.4" ],
]

require 'json'
require 'rake'
require 'rake/clean'
require 'rubygems'
require 'zip'

buildDir    = 'build'
modFile     = "#{info[:name]}_#{info[:version]}"
translation = Hash.new

task :default => :build

desc "Build the mod"
task :build => [ :clean,
                 :initBuildDir,
                 :generateInfo,
                 :pack ]

task :initBuildDir do
  puts ' * Initialize build dir'
  FileUtils.mkdir_p(buildDir) unless File.exists?(buildDir)
end

task :generateInfo do
  puts ' * Generate info.json'
  info[:dependencies] << "base >= #{info[:factorio_version]}"
  dependencies.each do |dep|
    info[:dependencies] << "? #{dep[0]} >= #{dep[1]}"
  end
  IO.write(File.join("#{buildDir}", 'info.json'), JSON.generate(info))
end

task :pack do
  puts ' * Pack mod file'
  files = %w(changelog.txt LICENSE.md README.md)
  files.concat Dir[File.join('locale', 'de', "*")]
  Zip::File.open(File.join("#{buildDir}", "#{modFile}.zip"), Zip::File::CREATE) do |zip|
    zip.add(File.join("#{modFile}", 'info.json'), File.join("#{buildDir}", 'info.json'))
    files.each do |file|
      zip.add(File.join("#{modFile}", "#{file}"), "#{file}")
    end
  end
end

CLEAN.include(buildDir)
